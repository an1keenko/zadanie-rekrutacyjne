import styles from "./AboutUs.module.scss";
import Button from "../Button/Button";

import aboutUs from "../../assets/about-us.png";

function AboutUs({ handleClick }: { handleClick: (id: string) => void }) {
  return (
    <section className={styles.about} id="about">
      <img className={styles.about__img} src={aboutUs} alt="O nas" />
      <div className={styles.about__container}>
        <div className={styles.about__container__wrapper}>
          <span className={styles.about__container__wrapper__line} />
          <span className={styles.about__container__wrapper__description}>
            O nas
          </span>
        </div>
        <h4 className={styles.about__container__headline}>
          Jesteśmy agencją, która sprosta Twoim wymaganiom!
        </h4>
        <p className={styles.about__container__text}>
          Zajmujemy się wszelkimi działaniami w dziedzinie promowania stron www
          w sieci, zakładania prostych wizytówek i rozbudowanych portali oraz
          nadzorowania ich rozwoju, funkcjonowania i treści, jakie się na nich
          pojawiają. Lubelska agencja marketingowa IBIF doskonale sprawdza się
          przy współpracy z firmami z całego kraju, komunikując się z klientami
          na wiele sposobów.
        </p>
        <Button onClick={() => handleClick("contact")}>Skontaktuj się</Button>
      </div>
    </section>
  );
}

export default AboutUs;
