import React from "react";
import styles from "./Button.module.scss";

type ButtonProps = {
  children: React.ReactNode;
  variant?: "outline" | "filled";
  type?: "submit";
  onClick?: () => void;
};

function Button({ children, variant = "filled", type, onClick }: ButtonProps) {
  const buttonClass = `${styles.button} ${
    variant === "filled" ? styles.button : styles["button--outlined"]
  }`;

  return (
    <button type={type} className={buttonClass} onClick={onClick}>
      {children}
    </button>
  );
}

export default Button;
