import React, { ChangeEvent, FormEvent, useState } from "react";
import styles from "./Form.module.scss";
import Button from "../Button/Button";
import Star from "../Star/Star";

const Form: React.FC = () => {
  const [email, setEmail] = useState<string>("");
  const [phone, setPhone] = useState<string>("");
  const [subject, setSubject] = useState<string>("");
  const [message, setMessage] = useState<string>("");
  const [accept, setAccept] = useState<boolean>(false);

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    if (!accept) {
      alert("Proszę zaakceptować politykę prywatności");
      return;
    }
    alert("Wiadomość została wysłana");
  };

  const handleInputChange =
    (setState: React.Dispatch<React.SetStateAction<string>>) =>
    (
      e: ChangeEvent<
        HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
      >,
    ) => {
      setState(e.target.value);
    };

  return (
    <section className={styles["form-container"]} id="contact">
      <form onSubmit={handleSubmit} className={styles.form}>
        <h4 className={styles.form__title}>Formularz kontaktowy</h4>
        <p className={styles.form__description}>
          Napisz do nas, z chęcią odpowiemy na każde pytanie!
        </p>
        <label className={styles.form__label}>E-mail</label>
        <input
          type="email"
          value={email}
          onChange={handleInputChange(setEmail)}
          placeholder="Wpisz swój adres e-mail"
          className={styles.form__input}
          required
        />
        <label className={styles.form__label}>Telefon</label>
        <input
          type="tel"
          value={phone}
          onChange={handleInputChange(setPhone)}
          placeholder="Wpisz swój numer telefonu"
          className={styles.form__input}
          required
        />
        <label className={styles.form__label}>Temat</label>
        <select
          value={subject}
          onChange={handleInputChange(setSubject)}
          className={styles.form__select}
          required
        >
          <option value="">Wybierz z opcji</option>
          <option value="marketing">Marketing</option>
          <option value="webdesign">Web Design</option>
          <option value="seo">SEO</option>
        </select>
        <label className={styles.form__label}>Wiadomość</label>
        <textarea
          value={message}
          onChange={handleInputChange(setMessage)}
          placeholder="Treść wiadomości"
          className={styles.form__textarea}
          required
        />
        <div className={styles.form__checkbox}>
          <input
            type="checkbox"
            checked={accept}
            onChange={(e) => setAccept(e.target.checked)}
          />
          <label>Akceptuję politykę prywatności.</label>
        </div>
        <Button type="submit">Wyślij wiadomość</Button>
      </form>
      <div className={styles["form-container__stars"]}>
        <Star
          color="#DA1384"
          style={{ transform: " translate(-1300%, 100%)" }}
        />
        <Star style={{ transform: " translate(105%, 315%)" }} />
        <Star
          width={180}
          height={180}
          style={{ transform: "translate(-570%, 240%)" }}
        />
      </div>
    </section>
  );
};

export default Form;
