import styles from "./Card.module.scss";

type CardProps = {
  src: string;
  alt: string;
  title: string;
  description: string;
};

function Card({ src, alt, title, description }: CardProps) {
  return (
    <div className={styles.card}>
      <img className={styles.card__image} src={src} alt={alt} />
      <h4 className={styles.card__title}>{title}</h4>
      <p className={styles.card__description}>{description}</p>
    </div>
  );
}

export default Card;
