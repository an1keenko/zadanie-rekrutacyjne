import styles from "./Offers.module.scss";
import Card from "../Card/Card";

import sites from "../../assets/sites.png";
import shops from "../../assets/shops.png";
import positioning from "../../assets/positioning.png";

function Offers() {
  return (
    <div className={styles.offers} id="services">
      <div className={styles.offers__header}>
        <span className={styles.offers__header__line} />
        <span className={styles.offers__header__title}>Usługi</span>
      </div>
      <h4 className={styles.offers__subtitle}>Co możemy Ci zaoferować?</h4>
      <div className={styles.offers__cards}>
        <Card
          src={sites}
          alt="Strony www"
          title="Strony www"
          description="Budujemy przejrzyste, nowoczesne i estetyczne serwisy WWW dostosowane do urządzeń mobilnych."
        />
        <Card
          src={shops}
          alt="Sklepy internetowe"
          title="Sklepy internetowe"
          description="Funkcjonalne, użyteczne i responsywne sklepy to nasza specjalność."
        />
        <Card
          src={positioning}
          alt="Pozycjonowanie"
          title="Pozycjonowanie"
          description="Skutecznie przyciągamy uwagę Twoich potencjalnych klientów."
        />
      </div>
    </div>
  );
}

export default Offers;
