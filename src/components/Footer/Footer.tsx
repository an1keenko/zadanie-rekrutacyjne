import styles from "./Footer.module.scss";

function Footer() {
  return (
    <>
      <div className={styles.footer}>
        <img className={styles.footer__logo} src="./logo.png" alt="Logo" />
        <div className={styles.footer__contacts}>
          <div className={styles.footer__contacts__item}>
            <span className={styles.footer__contacts__item__label}>
              Telefon
            </span>
            <span className={styles.footer__contacts__item__circle} />
            <a
              className={styles.footer__contacts__item__value}
              href="tel:505-551-616"
            >
              505-551-616
            </a>
          </div>
          <div className={styles.footer__contacts__item}>
            <span className={styles.footer__contacts__item__label}>E-mail</span>
            <span className={styles.footer__contacts__item__circle} />
            <a
              className={styles.footer__contacts__item__value}
              href="mailto:biuro@ibif.pl"
            >
              biuro@ibif.pl
            </a>
          </div>
          <button
            className={styles.footer__contacts__button}
            onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
          />
        </div>
      </div>
      <p className={styles.realization}>
        realizacja:&nbsp;
        <a className={styles.realization__link} href="http://ibif.pl">
          ibif.pl
        </a>
      </p>
    </>
  );
}

export default Footer;
