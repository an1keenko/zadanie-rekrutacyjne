import styles from "./Main.module.scss";

import Button from "../Button/Button";
import Slider from "../../features/Slider/Slider";

import devices from "../../assets/devices.png";
import marketing from "../../assets/marketing.png";

function Main({ handleClick }: { handleClick: (id: string) => void }) {
  const images = [devices, marketing];

  return (
    <div className={styles.main}>
      <div className={styles.main__content}>
        <h4 className={styles.main__title}>Strony internetowe</h4>
        <h5 className={styles.main__subtitle}>
          Zapytaj o naszą ofertę e-commerce i rozwijaj swój biznes w sieci.
        </h5>
        <Button onClick={() => handleClick("contact")}>Bezpłatna wycena</Button>
      </div>
      <Slider images={images} />
    </div>
  );
}

export default Main;
