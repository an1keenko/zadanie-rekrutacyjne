import styles from "./Header.module.scss";
import Button from "../Button/Button";

function Header({ handleClick }: { handleClick: (id: string) => void }) {
  return (
    <header className={styles.header}>
      <img className={styles.header__img} src="./logo.png" alt="Logo" />
      <nav className={styles.header__nav}>
        <Button variant="outline" onClick={() => handleClick("about")}>
          O nas
        </Button>
        <Button variant="outline" onClick={() => handleClick("services")}>
          Usługi
        </Button>
        <Button variant="outline" onClick={() => handleClick("contact")}>
          Kontakt
        </Button>
        <Button onClick={() => handleClick("contact")}>Darmowa wycena</Button>
      </nav>
    </header>
  );
}

export default Header;
