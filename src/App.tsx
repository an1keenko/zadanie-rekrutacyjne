import React from "react";

import Header from "./components/Header/Header";
import Main from "./components/Main/Main";
import AboutUs from "./components/AboutUs/AboutUs";
import Offers from "./components/Offers/Offers";
import Form from "./components/Form/Form";
import Footer from "./components/Footer/Footer";

function App() {
  const handleClick = (id: string) => {
    const element = document.getElementById(id);
    if (element) {
      const rect = element.getBoundingClientRect();
      const scrollTop =
        window.pageYOffset || document.documentElement.scrollTop;
      window.scroll({
        top: rect.top + scrollTop - 80,
        behavior: "smooth",
      });
    }
  };

  return (
    <>
      <Header handleClick={handleClick} />
      <Main handleClick={handleClick} />
      <AboutUs handleClick={handleClick} />
      <Offers />
      <Form />
      <Footer />
    </>
  );
}

export default App;
