import React, { useState } from "react";
import styles from "./Slider.module.scss";

function Slider({ images }: { images: string[] }) {
  const [currentSlide, setCurrentSlide] = useState(0);

  const nextSlide = () => {
    if (currentSlide < images.length - 1) {
      setCurrentSlide(currentSlide + 1);
    }
  };

  const prevSlide = () => {
    if (currentSlide > 0) {
      setCurrentSlide(currentSlide - 1);
    }
  };

  return (
    <div className={styles.slider}>
      <img
        src={images[currentSlide]}
        alt={`slide ${currentSlide}`}
        className={styles.slider__slide}
      />
      <div className={styles.slider__buttons}>
        <button
          className={`${styles.slider__buttons__buttonPrev} ${
            currentSlide === 0 ? styles.buttonDisabled : ""
          }`}
          onClick={prevSlide}
          disabled={currentSlide === 0}
        />
        <button
          className={`${styles.slider__buttons__buttonNext} ${
            currentSlide === images.length - 1 ? styles.buttonDisabled : ""
          }`}
          onClick={nextSlide}
          disabled={currentSlide === images.length - 1}
        />
      </div>
    </div>
  );
}

export default Slider;
